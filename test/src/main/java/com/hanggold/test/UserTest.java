package com.hanggold.test;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-07-06 09:08
 * @since
 **/
public class UserTest {

    private Object condition;

    public Object getCondition() {
        return condition;
    }

    public void setCondition(Object condition) {
        this.condition = condition;
    }
}
