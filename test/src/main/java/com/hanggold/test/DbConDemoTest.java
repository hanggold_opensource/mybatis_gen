package com.hanggold.test;

import org.junit.Test;

import java.sql.*;

/**
 *
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2020-10-12 10:44
 * @since 1.0.0
 **/
public class DbConDemoTest {





	@Test
	public void oracleDemoOne() throws SQLException {
		Connection connection = getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("select * from  user_tab_comments");
		ResultSet resultSet = preparedStatement.executeQuery();
//		while (resultSet.next()){
//			String tableName = resultSet.getString("TABLE_NAME");
//			System.out.println("table: " + tableName);
//		}

		DatabaseMetaData databaseMetaData = connection.getMetaData();
		ResultSet resultSet1 = databaseMetaData.getColumns(null, null, "CORE_USERPROFILE", null);
		ResultSetMetaData metaData = resultSet1.getMetaData();
		int columnCount = metaData.getColumnCount();
		System.out.println("c: " + columnCount);

		for (int i=0;i<columnCount;i++){
			System.out.println("cn: "+ metaData.getColumnName(i+1));
		}

		while (resultSet1.next()){

			String columnName = resultSet1.getString("COLUMN_NAME");
			System.out.println("columnName: " + columnName);

			String remarks = resultSet1.getString("REMARKS");
			System.out.println("remarks: " + remarks);
		}
	}

	@Test
	public void primaryKeyDemo() throws SQLException {

		Connection connection = getConnection();
		ResultSet resultSet = connection.getMetaData().getPrimaryKeys(null, null, "CORE_USERPROFILE");

		ResultSetMetaData metaData = resultSet.getMetaData();
		int columnCount = metaData.getColumnCount();
		System.out.println("c: " + columnCount);

		while (resultSet.next()){
			System.out.println("name: " + resultSet.getString("COLUMN_NAME"));
		}
	}

	@Test
	public void uniqueDemo() throws SQLException {
		Connection connection = getConnection();
		ResultSet resultSet = connection.getMetaData().getIndexInfo(null, null, "CORE_USERPROFILE",true,false);

//		ResultSetMetaData metaData = resultSet.getMetaData();
//		int columnCount = metaData.getColumnCount();
//		System.out.println("c: " + columnCount);
//
//		for (int i=0;i<columnCount;i++){
//			System.out.println("cn: "+ metaData.getColumnName(i+1));
//		}

		while (resultSet.next()){
			System.out.println("name: " + resultSet.getString("COLUMN_NAME"));
		}
	}

	private Connection getConnection() {
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Connection connection = null;
		try {
			connection = DriverManager
					.getConnection("jdbc:oracle:thin:@172.17.125.12:1521/cmbdb","elearning","6Lyf13iXdXE7LRoA");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}
}
