package com.hanggold.mybatis.gen.dataloaders;

import com.hanggold.mybatis.gen.exec.TableAnalysisUtil;
import com.hanggold.mybatis.gen.model.Gen;
import com.hanggold.mybatis.gen.utils.ConfigUtil;

import java.sql.Connection;

/**
 * 这里写文档描述
 *
 * @author smc
 * @date 2018-09-10 16:35
 * @since
 */
public class GenAbstractDataLoader extends AbstractDataLoader {

	private static final String PAGE_FLAG = "yes";

	@Override
	public void gen(Gen gen, Connection connection) {

		gen.setDbType(ConfigUtil.getMybatisGenMojo().getDbType().toLowerCase());

		boolean validate = TableAnalysisUtil.ansTable(gen, connection);


		if (validate) {

			/**
			 * 创建DO数据
			 */
			TableAnalysisUtil.createDO(gen);

			TableAnalysisUtil.createDOQuery(gen);

			// 是否创建分页
			if (PAGE_FLAG.equalsIgnoreCase(ConfigUtil.getMybatisGenMojo().getPageFlag())) {
				TableAnalysisUtil.createPage(gen);
			}
			TableAnalysisUtil.createDOMapper(gen);
			TableAnalysisUtil.createXMLMapper(gen);
		}

		/**
		 *  1.1.0版本剔除dao代码的生成该代码比较鸡肋本身就没什么用
		 */
		//        TableAnalysisUtil.createDAO(gen);

	}


}
