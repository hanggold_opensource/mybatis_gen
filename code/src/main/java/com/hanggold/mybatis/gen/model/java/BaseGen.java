package com.hanggold.mybatis.gen.model.java;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-07-07 08:57
 * @since
 **/
public class BaseGen {
    /**
     * 是否已经存在文件
     */
    private String hasExistFile="no";

    public String getHasExistFile() {
        return hasExistFile;
    }

    public void setHasExistFile(String hasExistFile) {
        this.hasExistFile = hasExistFile;
    }
}
